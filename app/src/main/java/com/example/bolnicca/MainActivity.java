package com.example.bolnicca;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.prefs.Preferences;

public class MainActivity extends AppCompatActivity {
    SQLiteDatabase sqLiteDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedPreferences = getSharedPreferences("main",MODE_PRIVATE);
        boolean key = sharedPreferences.getBoolean("loginkey",false);
        if (key){
            Intent intent = new Intent(this,MainScreenUser.class);
            startActivity(intent);
        }
        setContentView(R.layout.activity_main);
        sqLiteDatabase = openOrCreateDatabase("users.db",MODE_PRIVATE,null);
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS users (fio VARCHAR(200), birthdate VARCHAR(30), age INT, passport VARCAHAR(50), " +
                "snils VARCHAR(30),number VARCHAR(30),email VARCHAR(100),login VARCHAR(100), password VARCHAR(100))");
        sqLiteDatabase.close();
    }

    public void pricesClick(View view) {
        Intent intent = new Intent(this,PricesActivity.class);
        startActivity(intent);
    }

    public void authorizeClick(View view) {
        Intent intent = new Intent(this,AuthorizationActivity.class);
        startActivity(intent);
    }

    public void registerClick(View view) {
        Intent intent = new Intent(this,RegistrationActivity.class);
        startActivity(intent);
    }

    public void newsClick(View view) {
        Intent intent = new Intent(this,NewsActivity.class);
        startActivity(intent);
    }
}