package com.example.bolnicca;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class RegistrationActivity extends AppCompatActivity {
    EditText login,password,passport,fio,snils,birthdate,email,number;
    SQLiteDatabase sqLiteDatabase;
    ContentValues row = new ContentValues();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        login = findViewById(R.id.loginTextfield);
        passport = findViewById(R.id.passportTextField);
        password = findViewById(R.id.passwordTextField);
        fio = findViewById(R.id.fioTextField);
        snils = findViewById(R.id.snilsTextField);
        birthdate = findViewById(R.id.birthdateTextField);
        email = findViewById(R.id.EmailAddressTextField);
        number = findViewById(R.id.phoneTextField);

    }

    public void registrationClick(View view) {
        sqLiteDatabase = openOrCreateDatabase("users.db",MODE_PRIVATE,null);
        row.put("fio",fio.getText().toString());
        row.put("login",login.getText().toString());
        row.put("password",password.getText().toString());
        row.put("passport",passport.getText().toString());
        row.put("email",email.getText().toString());
        row.put("birthdate",birthdate.getText().toString());
        row.put("number",number.getText().toString());
        row.put("snils",snils.getText().toString());
        sqLiteDatabase.insert("users",null,row);
        sqLiteDatabase.close();
        Intent intent = new Intent(this,AuthorizationActivity.class);
        startActivity(intent);
    }
}