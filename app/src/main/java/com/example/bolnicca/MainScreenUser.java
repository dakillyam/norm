package com.example.bolnicca;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainScreenUser extends AppCompatActivity {
    TextView fio;
    SQLiteDatabase sqLiteDatabase;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen_user);
        fio = findViewById(R.id.fiotextview);
        sharedPreferences  = getSharedPreferences("main",MODE_PRIVATE);

        sqLiteDatabase = openOrCreateDatabase("users.db",MODE_PRIVATE,null);
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT fio FROM users WHERE login =?",new String[]{sharedPreferences.getString("login","")});
        cursor.moveToNext();
        String fullfio = "Добро пожаловать!\n" + cursor.getString(cursor.getColumnIndex("fio"));
        fio.setText(fullfio);
        sqLiteDatabase.close();

    }

    public void profileClick(View view) {
        Intent intent = new Intent(this,ProfileActivity.class);
        startActivity(intent);
    }
}