package com.example.bolnicca;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.MyVH> {
    String[] newsdate,newstitle,newstext;

    public MyRecyclerAdapter(String[] newsdate, String[] newstitle, String[] newstext) {
        this.newsdate = newsdate;
        this.newstitle = newstitle;
        this.newstext = newstext;
    }

    @NonNull
    @Override
    public MyVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.newsrecycleritem,parent,false);
        return new MyVH(root);
    }

    @Override
    public void onBindViewHolder(@NonNull MyVH holder, int position) {
        holder.title.setText(newstitle[position]);
        holder.date.setText(newsdate[position]);
        holder.text.setText(newstext[position]);
    }

    @Override
    public int getItemCount() {
        return newsdate.length;
    }
    class MyVH extends RecyclerView.ViewHolder{
        TextView title,date,text;
        public MyVH(@NonNull View itemView) {
            super(itemView);
            text = itemView.findViewById(R.id.newstext);
            title = itemView.findViewById(R.id.newstitle);
            date = itemView.findViewById(R.id.newsdate);
        }
    }
}
