package com.example.bolnicca;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class ProfileActivity extends AppCompatActivity {
    TextView fio,login,snils,passport,birthdate;
    EditText password,email,number;
    SQLiteDatabase sqLiteDatabase;
    String log;
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        sharedPreferences = getSharedPreferences("main",MODE_PRIVATE);
        log = sharedPreferences.getString("login","danil");
        sqLiteDatabase = openOrCreateDatabase("users.db",MODE_PRIVATE,null);
        fio = findViewById(R.id.fioprofile);
        login = findViewById(R.id.loginprofile);
        snils = findViewById(R.id.snilsprofile);
        passport = findViewById(R.id.passportprofile);
        birthdate = findViewById(R.id.birthdateprofile);
        password = findViewById(R.id.passwordprofile);
        email = findViewById(R.id.emailprofile);
        number = findViewById(R.id.phoneprofile);
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM users WHERE login =?",new String[]{log});
        cursor.moveToNext();
        fio.setText("ФИО: " + cursor.getString(cursor.getColumnIndex("fio")));
        login.setText("Логин: " + cursor.getString(cursor.getColumnIndex("login")));
        snils.setText("СНИЛС: " + cursor.getString(cursor.getColumnIndex("snils")));
        passport.setText("Паспорт: " + cursor.getString(cursor.getColumnIndex("passport")));
        birthdate.setText("Дата рождения: " + cursor.getString(cursor.getColumnIndex("birthdate")));
        password.setText(cursor.getString(cursor.getColumnIndex("password")));
        email.setText(cursor.getString(cursor.getColumnIndex("email")));
        number.setText(cursor.getString(cursor.getColumnIndex("number")));
    }

    public void saveChangeclick(View view) {
        ContentValues row = new ContentValues();
        row.put("password",password.getText().toString());
        row.put("email",email.getText().toString());
        row.put("number",number.getText().toString());
        sqLiteDatabase.update("users",row,"login =?",new String[]{log});
        sqLiteDatabase.close();
    }

    public void exitClick(View view) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("loginkey",false);
        editor.putString("login","");
        editor.apply();
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }
}