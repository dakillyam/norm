package com.example.bolnicca;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class AuthorizationActivity extends AppCompatActivity {
    EditText loginedittext,passwordedittext;
    boolean key;
    SharedPreferences sharedPreferences;
    SQLiteDatabase sqLiteDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authorization);
        sharedPreferences = getSharedPreferences("main",MODE_PRIVATE);
        loginedittext = findViewById(R.id.loginTextfield);
        passwordedittext = findViewById(R.id.passwordTextField);
        sqLiteDatabase = openOrCreateDatabase("users.db",MODE_PRIVATE,null);

    }

    public void authorizationClick(View view) {
        if(!loginedittext.getText().toString().equals("") || !passwordedittext.getText().toString().equals(""))
        {
            Cursor cursor = sqLiteDatabase.rawQuery("SELECT password FROM users WHERE login =?",new String[]{loginedittext.getText().toString()});
            cursor.moveToNext();
            if (passwordedittext.getText().toString().equals(cursor.getString(cursor.getColumnIndex("password")))){
            key = true;
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean("loginkey",key);
            editor.putString("login",loginedittext.getText().toString());
            editor.apply();
            Intent intent = new Intent(this,MainScreenUser.class);
            startActivity(intent);
    }else {
                Toast.makeText(this,"Неправильный логин или пароль", Toast.LENGTH_LONG).show();
            }
        }
        else {
            Toast.makeText(this,"Заполните все поля", Toast.LENGTH_LONG).show();
        }
    }
}